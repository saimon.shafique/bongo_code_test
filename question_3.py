"""
Runtime requirements for lca:
Runtime requirements for get_ancestors is O(N) because for each input node there
are N number of recursions until the root is reached.
Since the function is being called twice, total time complexity for lca = O(N) * 2
"""


def lca(node_1, node_2):
    ancestors_1 = get_ancestors(node_1, [])
    ancestors_2 = get_ancestors(node_2, [])
    common_ancestors = [value for value in ancestors_1 if value in ancestors_2]
    print(common_ancestors[0])
    return common_ancestors[0]


def get_ancestors(n, ancestors):
    ancestors.append(n.value)
    if n.parent:
        return get_ancestors(n.parent, ancestors)
    else:
        return ancestors


if __name__ == "__main__":
    class Node(object):
        def __init__(self, value, parent):
            self.value = value
            self.parent = parent


    n1 = Node(1, None)
    n2 = Node(2, n1)
    n3 = Node(3, n1)
    n4 = Node(4, n2)
    n5 = Node(5, n2)
    n6 = Node(6, n3)
    n7 = Node(7, n3)
    n8 = Node(8, n4)
    n9 = Node(9, n4)

    lca(n3, n8)