from question_3 import lca


class Node(object):
    def __init__(self, value, parent):
        self.value = value
        self.parent = parent


def test_lca_1():
    n1 = Node(1, None)
    n2 = Node(2, n1)
    n3 = Node(3, n1)
    n4 = Node(4, n2)
    n5 = Node(5, n2)
    n6 = Node(6, n3)
    n7 = Node(7, n3)
    n8 = Node(8, n4)
    n9 = Node(9, n4)

    assert lca(n2, n3) == 1
    assert lca(n4, n5) == 2
    assert lca(n6, n7) == 3
    assert lca(n8, n9) == 4


def test_lca_2():
    n1 = Node(1, None)
    n2 = Node(2, n1)
    n3 = Node(3, n1)
    n4 = Node(4, n2)
    n5 = Node(5, n4)
    n6 = Node(6, n5)
    n7 = Node(7, n5)
    n8 = Node(8, n3)
    n9 = Node(9, n3)

    assert lca(n2, n3) == 1
    assert lca(n4, n5) == 4
    assert lca(n6, n7) == 5
    assert lca(n8, n9) == 3
