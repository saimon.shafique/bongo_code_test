def print_depth(input_dict):
    result = traverse_dict(input_dict=input_dict, result_list=[])
    for item in result:
        key, depth = item
        print(key, depth)
    return result


def traverse_dict(input_dict, result_list, depth=1):
    for key, val in input_dict.items():
        result_list.append((key, depth))
        if val:
            if isinstance(val, dict):
                traverse_dict(val, result_list, depth+1)
            else:
                try:
                    val = val.__dict__
                    traverse_dict(val, result_list, depth + 1)
                except AttributeError:
                    continue
    return result_list


if __name__ == "__main__":
    class Person(object):
        def __init__(self, first_name, last_name, father):
            self.first_name = first_name
            self.last_name = last_name
            self.father = father


    person_a = Person("user", "1", None)
    person_b = Person("user", "2", person_a)

    sample_dict = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4,
                "user": person_b
            }
        }
    }
    print_depth(sample_dict)
