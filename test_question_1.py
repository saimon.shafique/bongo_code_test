import pytest
from question_1 import print_depth


@pytest.mark.parametrize("sample_input,expected", [
    ({
         "key1": 1,
         "key2": {
             "key3": 1,
             "key4": {
                 "key5": 4
             }
         }
     },
     {
         "key1": 1,
         "key2": 1,
         "key3": 2,
         "key4": 2,
         "key5": 3
     }
    ),
    ({
         "key1": {
             "key2": 1,
             "key3": {
                 "key4": {
                     "key5": 3
                 }
             }
         },
         "key6": 4,
     },
     {
         "key1": 1,
         "key2": 2,
         "key3": 2,
         "key4": 3,
         "key5": 4,
         "key6": 1
     }
    ),
    ({
        "name": "Pacman",
        "address": {
            "street": "23/A",
            "lane": 23,
            "block": "a",
            "house": {
                "plot": 200,
                "flat": "C23"
            }
        },
    },
     {
         "name": 1,
         "address": 1,
         "street": 2,
         "lane": 2,
         "block": 2,
         "house": 2,
         "plot": 3,
         "flat": 3
     })

])
def test_depth(sample_input, expected):
    actual_dict = print_depth(sample_input)
    for key, value in expected.items():
        assert actual_dict[key] == value


