def print_depth(input_dict):
    result = traverse_dict(input_dict=input_dict, result={})
    for key, value in result.items():
        print(key, value)
    return result


def traverse_dict(input_dict, result, depth=1):
    for key, val in input_dict.items():
        result[key] = depth
        if val:
            if isinstance(val, dict):
                traverse_dict(val, result, depth+1)
    return result


if __name__ == "__main__":
    sample_dict = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4
            }
        }
    }
    print_depth(sample_dict)
