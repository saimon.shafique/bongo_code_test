import pytest
from question_2 import print_depth


class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father


person_a = Person("Grandpa", "Bear", "Great Grandpa")
person_b = Person("Papa", "Bear", person_a)
person_c = Person("Baby", "Bear", person_b)


@pytest.mark.parametrize("sample_input,expected", [
    ({
         "key1": 1,
         "key2": {
             "key3": 1,
             "key4": {
                 "key5": 4,
                 "user": person_c
             }
         }
     },
     [
         ("key1", 1),
         ("key2", 1),
         ("key3", 2),
         ("key4", 2),
         ("key5", 3),
         ("user", 3),
         ("first_name", 4),
         ("last_name", 4),
         ("father", 4),
         ("first_name", 5),
         ("last_name", 5),
         ("father", 5),
         ("first_name", 6),
         ("last_name", 6),
         ("father", 6),
     ]
    )
])
def test_depth(sample_input, expected):
    actual_list = print_depth(sample_input)
    assert set(actual_list) == set(expected)


